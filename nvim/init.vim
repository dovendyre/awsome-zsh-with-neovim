call plug#begin()
Plug 'tpope/vim-sensible' " sets some normal standards
Plug 'vim-airline/vim-airline' " status bar
Plug 'vim-airline/vim-airline-themes' "status bar theme
Plug 'sheerun/vim-polyglot' " language packs
Plug 'morhetz/gruvbox' " color theme
Plug 'valloric/youcompleteme' " autoclompletion
Plug 'nvie/vim-flake8' " python syntax and style checker
Plug 'vim-syntastic/syntastic' " syntax checkings
Plug 'preservim/nerdtree' " file finder
Plug 'tiagofumo/vim-nerdtree-syntax-highlight' " file finder syntax highlight
Plug 'Xuyuanp/nerdtree-git-plugin' " Git plugin for nerdtree
Plug 'raimondi/delimitmate' " auto closing brackets/quotes/...
Plug 'lambdalisue/suda.vim' " sudo saving using suda
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
call plug#end()

let $FZF_DEFAULT_COMMAND='find . \! \( -type d -path ./.git -prune \) \! -type d \! -name ''*.tags'' -printf ''%P\n'''

" Setting leader key
let mapleader = ","

" Leader timeout
set showcmd

" General settings
set t_Co=256
set tabstop=2
set shiftwidth=2
set smarttab
set smartindent
set autoindent
set updatetime=300
set timeoutlen=300
set clipboard=unnamedplus
" Remove highlighting from search
nnoremap <nowait><leader>h :noh<CR>
set hlsearch

" ,+p,i = Install plugins
nnoremap <leader>pi :PlugInstall<CR>
" ,+p+u = Update plugins
nnoremap <leader>pu :PlugUpdate<CR>

" Set paste mode as default mode
" set paste
" Toogle pastemod with ,+p
nnoremap <leader>p :se invpaste paste?<CR>

" Fuzzy filefinder
nnoremap <C-f> <ESC><ESC>:Files<CR>
nnoremap <leader>f <ESC><ESC>:Files<CR>
nnoremap <leader>sf <ESC><ESC>:Files /<CR>
inoremap <C-f> <ESC><ESC>:BLines!<CR>
nnoremap <C-b> <ESC><ESC>:Buffers!<CR>
nnoremap <leader>b <ESC><ESC>:Buffers!<CR>
nnoremap <C-g> <ESC><ESC>:BCommits!<CR>

" Quality of life
" ,+w save the file
nnoremap <nowait><leader>w :w<CR>
" ,+q force quit
nnoremap <leader>q :q!<CR>
" ,+w+q write and quit
nnoremap <leader>wq :wq<CR>
" ,+w+q write and quit
nnoremap <nowait><leader>wf :SudaWrite<CR>

" Reread the config file
nnoremap <leader>sv :source ~/.config/nvim/init.vim<CR>

" Nerdtree
" Open file explorer
nnoremap <leader>t :NERDTreeToggle<CR>
" Close NerdTree on openeing file
let NERDTreeQuitOnOpen = 1

" Move between buffers
" Move to next buffer
nnoremap <leader>j :bnext<CR>
" Move to previous buffer
nnoremap <leader>k :bprevious<CR>
" Close buffer
nnoremap <leader>c :bd<CR>
nnoremap <C-w> :bd<CR>

" Quality of life
inoremap II <Esc>I
inoremap AA <Esc>A
inoremap OO <Esc>O
inoremap CC <Esc>C
inoremap SS <Esc>S
inoremap DD <Esc>dd
inoremap UU <Esc>u
inoremap PP <Esc>p

nnoremap <leader>m :call ToggleMouse()<CR>

function! ToggleMouse()
    " check if mouse is enabled
    if &mouse == 'a'
        " disable mouse
        set mouse=
    else
        " enable mouse everywhere
        set mouse=a
    endif
endfunc

" Make tab usable in vistual mode
:vmap <Tab> >
:vmap <S-Tab> <

" set backgrounds
set background=dark
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set termguicolors
colorscheme gruvbox

" settings for the airline status bar
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_theme='tomorrow'

" enable line numbers
" turn hybrid line numbers on
set number relativenumber

" Added hotkey to disable and enable line numbers
nnoremap <leader>n :call ToggleNumber()<CR>    

function! ToggleNumber()
    " check if mouse is enabled
    if &number
        " disable mouse
        set nonumber norelativenumber
    else
        " enable mouse everywhere
        set number relativenumber
    endif
endfunc


" remove trailing whitespace in python files on save
autocmd BufWritePre *.py :%s/\s\+$//e

" all extra windows pop up at the bottom
set splitbelow

" set python directories
let g:python_host_prog = '/usr/bin/python2.7'
let g:python3_host_prog = '/usr/bin/python3'

" persistent undo
set undodir=~/.config/nvim/undodir
set undofile

