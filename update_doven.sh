#!/bin/sh

echo " "        
echo "#############################################################"        
echo "This will update awsome-zsh-with-neovim on your computer "        
echo " "        
echo "Please follow instructions... Use at own risk..... "        
echo " "        
echo "#############################################################"


### Henter ut i hvilken mappe scriptet ligger
scriptdir=$(dirname $(readlink -f $0))

echo -n "Does this user have sudo access? (y/n)"
old_stty_cfg=$(stty -g)
stty raw -echo ; answer=$(head -c 1) ; stty $old_stty_cfg # Careful playing with stty
if echo "$answer" | grep -iq "^y" ;then
    sudoaccess=1
else
    sudoaccess=2
fi

## Make a local BIN dir if not existing
[ ! -d ~/.local/bin ] && mkdir ~/.local/bin

# Backups the origial config files
[ -f ~/.zshrc ] && [ ! -f ~/.zshrc_org ] && mv ~/.zshrc ~/.zshrc_org
[ -f ~/.p10k.zsh ] && [ ! -f ~/.p10.zsh_org ] && mv ~/.p10k.zsh ~/.p10k.zsh_org

# Make config dir if not existing
[ -d ~/.awsomeZSH ] && rm -rf ~/.awsomeZSH && mkdir ~/.awsomeZSH

# Cloning the plugins into the plugin folder
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.awsomeZSH/powerlevel10k
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.awsomeZSH/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.awsomeZSH/zsh-syntax-highlighting
git clone https://github.com/ohmyzsh/ohmyzsh.git ~/.awsomeZSH/ohmyzsh
[ -d ~/.fzf ] && rm -rf ~/.fzf
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf && ~/.fzf/install --key-bindings --completion --no-update-rc

# Setting right permissions on .awsomeZSH folder
[ -d ~/.awsomeZSH ] && chmod -R 755 ~/.awsomeZSH

# Copy the conf file to the right place
cp -a $scriptdir/config/.zshrc ~/
cp -a $scriptdir/config/.p10k.zsh ~/

# Legge inn LS_COLORS
[ ! -d ~/.local/share ] && mkdir -p ~/.local/share
mkdir -p $scriptdir/tmp/LS_COLORS && curl -L https://api.github.com/repos/trapd00r/LS_COLORS/tarball/master | tar xzf - --directory=$scriptdir/tmp/LS_COLORS --strip=1
cd $scriptdir/tmp/LS_COLORS && sh install.sh
cd $scriptdir
rm -rf $scriptdir/tmp
# Legge inn LS_COLORS

# What editor you want to use?
echo -n "Want to set NeoVIM as your default editor (y/n)? | Answering NO will set NANO as your text editor! "
old_stty_cfg=$(stty -g)
stty raw -echo ; answer=$(head -c 1) ; stty $old_stty_cfg # Careful playing with stty
if echo "$answer" | grep -iq "^y" ;then
    echo " "
    echo "#####################################"
    echo "Setting NeoVIM as your default editor"
    echo "#####################################"
    ## NeoVIM 
    cd ~/.appimage && curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage && chmod u+x nvim.appimage && cd $scriptdir
    ln -s ~/.appimage/nvim.appimage ~/.local/bin/nvim
    sed -i "/export EDITOR=/c\export EDITOR='nvim'" ~/.zshrc
    sed -i "/export VISUAL=/c\export VISUAL='nvim'" ~/.zshrc
    sed -i "/alias n=/c\alias n=\"nvim\"" ~/.zshrc
    # Moved nvim dir if exist
    [ -d ~/.config/nvim ] && [ ! -d ~/.config/nvim_org ] && mv ~/.config/nvim ~/.config/nvim_org
    # Copy NeoVIM config
    cp -R $scriptdir/nvim ~/.config/

    # Installing Vim-Plug for NeoVIM
    [ ! -f ~/.local/share/nvim/site/autoload/plug.vim ] && curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

    # Setting up NeoVIM
    # Installing plugins
    ~/.local/bin/nvim --headless +"PlugInstall" +"q" +"q"
    cd ~/.local/share/nvim/plugged/youcompleteme && python3 install.py
    cd $scriptdir
else
    echo " "
    echo "###################################"
    echo "Setting nano as your default editor"
    echo "###################################"
    ## Nano
    sed -i "/export EDITOR=/c\export EDITOR='nano'" ~/.zshrc
    sed -i "/export VISUAL=/c\export VISUAL='nano'" ~/.zshrc
    sed -i "/alias n=/c\alias n=\"nano\"" ~/.zshrc
fi

# Want to install and configure tmux?
echo -n "Want to install tmux and my awsome tmux config (y/n)"
old_stty_cfg=$(stty -g)
stty raw -echo ; answer=$(head -c 1) ; stty $old_stty_cfg # Careful playing with stty
if echo "$answer" | grep -iq "^y" ;then
    mkdir $scriptdir/tmp && git clone https://gitlab.com/dovendyre/awsome-tmux-config.git $scriptdir/tmp && $scriptdir/tmp/install.sh && rm -rf $scriptdir/tmp
fi

# Setting default shell to #!/usr/bin/env zsh
[ $sudoaccess = 1 ] && chsh -s /bin/zsh
[ ! $sudoaccess = 1 ] && echo "A admin of your machine needs to run the following command: 'chsh --shell /bin/zsh username' to change default shell to zsh"

echo " "
echo "Done!!"
echo "You need to reboot your computer or reload your shell"
echo " "
