# Awsome ZSH with NeoVIM    

My zsh setup with powerlevel10k and a simple NeoVIM setup.

There is allso some handy aliases tailored to ArcoLinux. Aliases for debian planed..

Feel free to tailor this for you own needs!

CAUTION! Use at own risk!



## Installation    
```    
git clone https://gitlab.com/dovendyre/awsome-zsh-with-neovim.git && bash awsome-zsh-with-neovim/doven_install.sh    
```   



# NeoVIM keyboard shortcuts


| Shortcut            | Description                           |
| ------------------- | ------------------------------------- |
| *Leaderkey*         | ,                                     |
| *Leader + h*        | Removes the highligting of text       |
| *Leader + n*        | Toggle linenumbers                    |
| *Leader + m*        | Toggle mouse support                  |
| *Lader + t*         | Opens nerdtree filefinder             |
| *Lader + f*         | Opens fuzzy filefinder                |
| *Ctrl + f*          | Opens fuzzy filefinder                |
| *Lader + sf*        | Opens fuzzy filefinder and start search at / |
| *Lader + j*         | Move to next buffer                   |
| *Lader + k*         | Move to previous buffer               |
| *Lader + c*         | Close current buffer                  |
| *Lader + p*         | Toggle paste mode on/off              |
| *Lader + pi*        | Installs plugins listed in the init.vim config file |
| *Lader + pu*        | Update plugins listed in the init.vim config file |
| *Lader + w*         | Saves current file                    |
| *Lader + wf*        | Saves current file with admin rights (sudoa) |
| *Lader + wq*        | Saves current file and quits          |
| *Lader + sv*        | Reloads the init.vim config file      |
| *YY*                | Yank to then of the line      |

| Inser Mode Shortcut            | Description                           |
| ------------------------------ | ------------------------------------- |
| *Ctrl + f*                   | Serch the file for a string with fuzzy file finder |
| *II*                   | Go to just before the first non-blank text of the line  |
| *AA*                   | Go to the end of the line       |
| *OO*                   | Start editing on a new line above the current line   |
| *CC*                   | Rchange what is on the right of the curso     |
| *SS*                   | Change the whole line       |
| *DD*                   | Delete the current line (end in normal mode)       |
| *UU*                   | Undo |


# Thanks!

## Powerlevel10k
https://github.com/romkatv/powerlevel10k

## zsh-autosuggestions
https://github.com/zsh-users/zsh-autosuggestions

## zsh-syntax-highlighting
https://github.com/zsh-users/zsh-syntax-highlighting

## Oh My Zsh
https://github.com/ohmyzsh/ohmyzsh

## Neovim
https://github.com/neovim/neovim

## Tmux and tmux configuration
https://github.com/tmux/tmux
https://gitlab.com/dovendyre/awsome-tmux-config
