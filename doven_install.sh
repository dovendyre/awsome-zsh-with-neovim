#!/bin/bash

### Henter ut i hvilken mappe scriptet ligger
declare scriptdir=$(dirname $(readlink -f $0))

####################
##### Functions ####
####################

sudoaccess () {
    # Instellere zsh og avhengingeheter

    ## Debian based distro
    [ -f /usr/bin/apt ] && sudo apt update
    [ -f /usr/bin/apt ] && sudo apt install -y zsh neofetch cmake build-essential curl wget bat fd-find

    ## Arch based distro
    [ -f /usr/bin/pacman ]&& sudo pacman -Syy
    [ -f /usr/bin/pacman ] && sudo pacman --needed -S --noconfirm zsh wget neofetch bat fd

}

neovim () {
    echo $sudoaccess
    ## NeoVIM 
    [ -f /usr/bin/apt ] && [ $sudoaccess = 1 ] && sudo apt install -y python3-dev python3-pynvim xclip fuse
    [ -f /usr/bin/pacman ] && [ $sudoaccess = 1 ] && sudo pacman --needed -S --noconfirm python-pynvim xclip

    ## Making .appimage folder if not exist
    [ ! -d ~/.appimage ] && mkdir ~/.appimage
    cd ~/.appimage && curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage && chmod u+x nvim.appimage && cd $scriptdir

    ## Making it a global executable
    ln -s ~/.appimage/nvim.appimage ~/.local/bin/nvim

    ## Set neovim as default editor
    sed -i "/export EDITOR=/c\export EDITOR='nvim'" ~/.zshrc
    sed -i "/export VISUAL=/c\export VISUAL='nvim'" ~/.zshrc
    sed -i "/alias n=/c\alias n=\"nvim\"" ~/.zshrc

    # Moved nvim dir if exist
    [ -d ~/.config/nvim ] && [ ! -d ~/.config/nvim_org ] && mv ~/.config/nvim ~/.config/nvim_org

    # Copy NeoVIM config
    cp -R $scriptdir/nvim ~/.config/

    # Installing Vim-Plug for NeoVIM
    curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

    # Setting up NeoVIM
    # Installing plugins
    ~/.local/bin/nvim --headless +"PlugInstall" +"q" +"q"
    cd ~/.local/share/nvim/plugged/youcompleteme && python3 install.py
    cd $scriptdir
}

nano () {
    echo $sudoaccess
    ## Nano
    [ -f /usr/bin/apt ] && [ $sudoaccess = 1 ] && sudo apt install -y nano
    [ -f /usr/bin/pacman ] && [ $sudoaccess = 1 ] && sudo pacman -S --noconfirm nano 
    sed -i "/export EDITOR=/c\export EDITOR='nano'" ~/.zshrc
    sed -i "/export VISUAL=/c\export VISUAL='nano'" ~/.zshrc
    sed -i "/alias n=/c\alias n=\"nano\"" ~/.zshrc
}

tmux () {
    [ -f /usr/bin/apt ] && [ $sudoaccess = 1 ] && sudo apt install -y tmux 
    [ -f /usr/bin/pacman ] && [ $sudoaccess = 1 ] && sudo pacman -S --noconfirm tmux 
    mkdir $scriptdir/tmp && git clone https://gitlab.com/dovendyre/awsome-tmux-config.git $scriptdir/tmp && $scriptdir/tmp/install.sh && rm -rf $scriptdir/tmp
}


echo " "        
echo "#############################################################"        
echo "This will install my awsome-zsh-with-neovim on your computer "        
echo " "        
echo "Please follow instructions... Use at own risk..... "        
echo " "        
echo "#############################################################"


## Queuing what to do...
declare to_install=()

## Using a laptop and want to lock screen when you close the lid?
while true; do
    echo " "
    echo    "#####################################################################"
    read -p "Does this user have sudo access? (y/n) " yn

    case $yn in 
        [jJyY] ) echo ok, fixing...;
            to_install+=("sudoaccess")
            declare sudoaccess=1;
            break;;
        [nN] ) echo skipping...;
            declare sudoaccess=0;
            break;;
        * ) echo invalid response;;
    esac

done

while true; do
    echo " "
    echo    "#####################################################################"
    read -p "Want to set NeoVIM as your default editor (y/n)? | Answering NO will set NANO as your text editor! " yn

    case $yn in 
        [jJyY] ) echo ok, neovim it is...;
            to_install+=("neovim")
            break;;
        [nN] ) echo ok, nano it is...;
            to_install+=("nano")
            break;;
        * ) echo invalid response;;
    esac

done

while true; do
    echo " "
    echo    "#####################################################################"
    read -p "Want to install tmux and my awsome tmux config (y/n) " yn

    case $yn in 
        [jJyY] ) echo ok, neovim it is...;
            to_install+=("tmux")
            break;;
        [nN] ) echo skipping...;
            break;;
        * ) echo invalid response;;
    esac

done

for install_this in "${to_install[@]}"
do
    $install_this
done

## Make a local BIN dir if not existing
[ ! -d ~/.local/bin ] && mkdir ~/.local/bin

# Backups the origial config files
[ -f ~/.zshrc ] && [ ! -f ~/.zshrc_org ] && mv ~/.zshrc ~/.zshrc_org
[ -f ~/.p10k.zsh ] && [ ! -f ~/.p10.zsh_org ] && mv ~/.p10k.zsh ~/.p10k.zsh_org

# Make config dir if not existing
[ -d ~/.awsomeZSH ] && rm -rf ~/.awsomeZSH && mkdir ~/.awsomeZSH

# Cloning the plugins into the plugin folder
## Powerlevel 10k
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.awsomeZSH/powerlevel10k

## Autosuggestions
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.awsomeZSH/zsh-autosuggestions

## Syntax highlight
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.awsomeZSH/zsh-syntax-highlighting

## Ohmyzsh
git clone https://github.com/ohmyzsh/ohmyzsh.git ~/.awsomeZSH/ohmyzsh

## Removing old .fzf
[ -d ~/.fzf ] && rm -rf ~/.fzf
## Installing new fzf
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf && ~/.fzf/install --key-bindings --completion --no-update-rc

# Setting right permissions on .awsomeZSH folder
[ -d ~/.awsomeZSH ] && chmod -R 755 ~/.awsomeZSH

# Copy the conf file to the right place
cp -a $scriptdir/config/.zshrc ~/
cp -a $scriptdir/config/.p10k.zsh ~/

# Legge inn LS_COLORS
[ ! -d ~/.local/share ] && mkdir -p ~/.local/share
mkdir -p $scriptdir/tmp/LS_COLORS && curl -L https://api.github.com/repos/trapd00r/LS_COLORS/tarball/master | tar xzf - --directory=$scriptdir/tmp/LS_COLORS --strip=1
cd $scriptdir/tmp/LS_COLORS && sh install.sh
cd $scriptdir
rm -rf $scriptdir/tmp

# Setting default shell to #!/usr/bin/env zsh
[ $sudoaccess = 1 ] && chsh -s /bin/zsh
[ ! $sudoaccess = 1 ] && echo "A admin of your machine needs to run the following command: 'chsh --shell /bin/zsh username' to change default shell to zsh"

echo " "
echo "Done!!"
echo "You need to reboot your computer or reload your shell"
echo " "
